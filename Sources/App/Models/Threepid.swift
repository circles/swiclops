import Vapor
import Fluent

struct Threepid: Codable {
    var addedAt: UInt
    var address: String
    var medium: String
    var validatedAt: UInt

    enum CodingKeys: String, CodingKey {
        case addedAt = "added_at"
        case address
        case medium
        case validatedAt = "validated_at"
    }

    public init(medium: String, address: String, addedAt: UInt, validatedAt: UInt) {
        self.medium = medium
        self.address = address
        self.addedAt = addedAt
        self.validatedAt = validatedAt
    }

    public init(_ email: UserEmailAddress) {
        self.medium = "email"
        self.address = email.email
        let timestamp = email.lastUpdated ?? Date()
        self.addedAt = UInt(timestamp.timeIntervalSince1970)
        self.validatedAt = UInt(timestamp.timeIntervalSince1970)
    }
}
