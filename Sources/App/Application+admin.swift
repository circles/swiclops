//
//  Application+admin.swift
//
//
//  Created by Charles Wright on 10/23/23.
//

import Vapor




struct AdminBackendKey: StorageKey {
    typealias Value = SynapseAdminBackend
}

class SynapseAdminBackend: LifecycleHandler {
    var creds: MatrixCredentials?
    var sharedSecret: String
    
    init(creds: MatrixCredentials? = nil, sharedSecret: String) {
        self.creds = creds
        self.sharedSecret = sharedSecret
    }
    
    public func login(app: Application) async throws -> MatrixCredentials {
        
        // This only works if we have a MatrixConfig in the application's config
        guard let config = app.config
        else {
            app.logger.error("login failed - Can't log in without a config")
            throw Abort(.internalServerError)
        }
        
        guard let username = config.adminBackend.username,
              let password = config.adminBackend.password
        else {
            app.logger.error("login failed - Can't log in without username and password")
            throw Abort(.internalServerError)
        }
              
        let homeserver = config.matrix.homeserver
        let domain = config.matrix.domain

        let requestBody = LoginRequestBody(identifier: .init(type: "m.id.user", user: username),
                                           type: "m.login.password",
                                           password: password)
        
        let uri = URI(scheme: homeserver.scheme,
                      host: homeserver.host,
                      port: homeserver.port,
                      path: "/_matrix/client/v3/login")
        
        let headers = HTTPHeaders([
            ("Content-Type", "application/json"),
            ("Accept", "application/json")
        ])
        
        app.logger.debug("Sending login request for admin creds")
        let response = try await app.client.post(uri, headers: headers, content: requestBody)

        guard response.status == .ok
        else {
            app.logger.error("Login failed - got HTTP \(response.status.code) \(response.status.reasonPhrase)")
            throw MatrixError(status: response.status, errcode: .unauthorized, error: "Login failed")
        }
        
        let decoder = JSONDecoder()
        guard let buffer = response.body,
              let creds = try? decoder.decode(MatrixCredentials.self, from: buffer)
        else {
            app.logger.error("Failed to parse admin credentials")
            throw MatrixError(status: .internalServerError, errcode: .badJson, error: "Failed to get admin credentials")
        }
        
        app.logger.debug("Login success!")
        app.logger.debug("user_id: \(creds.userId)\tdevice_id: \(creds.deviceId)\taccess_token: \(creds.accessToken)")
        self.creds = creds

        return creds
    }
    
    public func logout(app: Application) async throws {
        if let creds = self.creds {
            guard let config = app.config
            else {
                app.logger.error("logout() - Failure - Can't get application config")
                throw Abort(.internalServerError)
            }
            
            let homeserver = config.matrix.homeserver
            
            let url = URI(scheme: homeserver.scheme, host: homeserver.host, port: homeserver.port, path: "/_matrix/client/v3/logout")
            let headers = HTTPHeaders([
                ("Accept", "application/json"),
                ("Authorization", "Bearer \(creds.accessToken)")
            ])
            app.logger.debug("logout() - Sending /logout request")
            let response = try await app.client.post(url, headers: headers)
            if response.status == .ok {
                app.logger.debug("logout() - Success")
            } else {
                app.logger.error("logout() - Failure - Received HTTP \(response.status.code) \(response.status.reasonPhrase)")
            }
        } else {
            app.logger.debug("logout() - No need to log out - We don't have any creds")
        }
    }
    
    func willBoot(_ app: Application) throws {
        app.admin = self
    }
    
    func didBoot(_ app: Application) throws {
        Task {
            var creds: MatrixCredentials?
            repeat {
                creds = try? await self.login(app: app)
                if creds != nil {
                    app.logger.debug("Logged in successfully")
                } else {
                    app.logger.debug("Login failed - Sleeping before retry")
                    try await Task.sleep(for: .seconds(30))
                }
            } while creds == nil
        }
    }
    
    func shutdown(_ app: Application) {
        Task {
            try await self.logout(app: app)
        }
    }
}

extension Application {
    
    var admin: SynapseAdminBackend? {
        get {
            self.storage[AdminBackendKey.self]
        }
        set {
            self.storage[AdminBackendKey.self] = newValue
        }
    }

}

// MARK: User Admin API
// https://element-hq.github.io/synapse/latest/admin_api/user_admin_api.html
extension SynapseAdminBackend {

    public func modifyUser(userId: String,
                           password: String? = nil,
                           logoutDevices: Bool? = nil,
                           displayname: String? = nil,
                           avatarUrl: String? = nil,
                           emails: [String]? = nil,
                           admin: Bool? = nil,
                           deactivated: Bool? = nil,
                           userType: String? = nil,
                           locked: Bool? = nil,
                           for req: Request
    ) async throws {

        guard let config = req.application.config
        else {
            let msg = "Could not find configuration"
            req.logger.error("Could not find configuration")
            throw MatrixError(status: .internalServerError, errcode: .unknown, error: msg)
        }
        let homeserver = config.matrix.homeserver
        let uri = URI(scheme: homeserver.scheme, host: homeserver.host, port: homeserver.port, path: "/_synapse/admin/v2/users/\(userId)")

        guard let accessToken = self.creds?.accessToken
        else {
            let msg = "Unable to authenticate to admin backend"
            req.logger.error("Unable to authenticate to admin backend")
            throw MatrixError(status: .internalServerError, errcode: .unknown, error: msg)
        }
        let headers = HTTPHeaders([
            ("Authorization", "Bearer \(accessToken)"),
            ("Content-Type", "application/json"),
            ("Accept", "application/json"),
        ])

        struct ShortThreepid: Codable {
            var medium: String
            var address: String
        }
        struct RequestBody: Content {
            var password: String?
            var logoutDevices: Bool?
            var displayname: String?
            var avatarUrl: String?
            var threepids: [ShortThreepid]?
            var admin: Bool?
            var deactivated: Bool?
            var userType: String?
            var locked: Bool?

            enum CodingKeys: String, CodingKey {
                case password
                case logoutDevices = "logout_devices"
                case displayname
                case avatarUrl = "avatar_url"
                case threepids
                case admin
                case deactivated
                case userType = "user_type"
                case locked
            }
        } // end struct RequestBody

        let body = RequestBody(password: password,
                               logoutDevices: logoutDevices,
                               displayname: displayname,
                               avatarUrl: avatarUrl,
                               threepids: emails?.map { ShortThreepid(medium: "email", address: $0) },
                               admin: admin,
                               deactivated: deactivated,
                               userType: userType,
                               locked: locked)

        let response = try await req.client.put(uri, headers: headers, content: body)
        guard response.status == .ok
        else {
            req.logger.error("Admin request rejected with status \(response.status)")
            throw Abort(.internalServerError)
        }

        req.logger.debug("Successfully updated user \(userId) on the homeserver")
    }
}