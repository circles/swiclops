#!/bin/bash

TAG=$1
REPO="gitlab.futo.org:5050/circles/swiclops"

time docker build -t "$REPO:$TAG" . &&
docker push -a $REPO
date

